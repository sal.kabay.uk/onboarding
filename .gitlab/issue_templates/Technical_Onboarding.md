### Overview
The features provided by the Defend Team are mostly leveraging tools that are executed during pipelines, environments, and the [GitLab Cluster Integration](https://docs.gitlab.com/ee/user/project/clusters/).

This document will guide you through the whole process. Don't hesitate to ask questions in Slack if anything is unclear:

- `#s_defend` for things related to Defend
- `#questions` for any general questions
- `#gdk` for [GDK](https://gitlab.com/gitlab-org/gitlab-development-kit) related questions

Enjoy!

Need some help? Ping one of your Defend teammates, I'm sure they will be more than willing to answer your call! You can find out who they are and where they at the [Better Team Page](https://leipert-projects.gitlab.io/better-team-page/?search=departments%3ADefend%20Section).

#### General
1. [ ] The [Defend Group](https://gitlab.com/gitlab-org/defend) addresses members of the Defend team and to keep other projects which do not directly affect the product. For instance the [onboarding project](https://gitlab.com/gitlab-org/defend/onboarding).
2. [ ] Check the [Secure & Defend Glossary](https://about.gitlab.com/handbook/engineering/development/secure/glossary-of-terms/) to get used to the terms you will frequently hear.

### Day 6: Setup

<details>
<summary>Click to expand/contract</summary>


#### Accounts

<details>
<summary>Click to expand/contract</summary>

1. [ ] Manager: Add the new member to [https://staging.gitlab.com/groups/defend-team-test](https://staging.gitlab.com/groups/defend-team-test) as Maintainer
1. [ ] Manager: Add the new member to [https://gitlab.com/groups/gitlab-org/defend/-/group_members](https://gitlab.com/groups/gitlab-org/defend/-/group_members) as Maintainer
1. [ ] Manager: Add the new member to [Geekbot](https://geekbot.io/)
1. [ ] Manager: Add the new member to the [Defend Stage Calendar](https://calendar.google.com/calendar/embed?src=gitlab.com_ed6207uel78de0j1849vjjnb3k%40group.calendar.google.com) and explain what it is.
1. [ ] New team member: Generate a [license](https://about.gitlab.com/handbook/developer-onboarding/#working-on-gitlab-ee) or ask your manager to do so if you don't have access to dev.gitlab.org
1. [ ] New team member: **(Prerequisite for the day 7 tasks)** Create a [new access request](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Single+Person+Access+Request) using the `Single Person Access Request` template for the following items:
    - **Google Group** Add to `defend-be` group,  you should be able to access our [Google Cloud Platform project](https://console.cloud.google.com/home/dashboard?folder=&organizationId=&project=group-defend-c8e44e)
1. [ ] New team member: Sign-in on [Sentry](https://sentry.gitlab.net/gitlab/) and join the [`#stage-defend` team](https://sentry.gitlab.net/settings/gitlab/teams/).

</details>

#### GDK

<details>
<summary>Click to expand/contract</summary>

1. [ ] Consider watching [video on GDK installation](https://www.youtube.com/watch?v=gxn-0KSfNaU).
1. [ ] Follow the [Getting
   started](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/master/README.md#getting-started) GDK guide.
1. [ ] Upload your Ultimate license key for GitLab Enterprise Edition by visiting `http://<your-gitlab-ee-url>:<your-gitlab-ee-port>/admin/license/new`
1. [ ] If you’re stuck, ask for help in [`#development`](https://gitlab.slack.com/archives/C02PF508L), [`#gdk`](https://gitlab.slack.com/archives/C2Z9A056E) Slack channels. Use Slack search for your questions first with filter `in:#gdk`. There is a possibility that someone has already had a similar issue.
1. [ ] Alternatively, you can install [gitlab-compose-kit](https://gitlab.com/gitlab-org/gitlab-compose-kit)
       if you’re familiar with Docker and running on Linux (performance makes macOS less feasible).
1. [ ] GDK contains a collection of resources that help running an instance of GitLab locally as well as
       GitLab codebase itself. Code of EE or CE version can be found in `/gitlab` folder of GDK.
       Check this folder.

</details>

#### Runner

<details>
<summary>Click to expand/contract</summary>

1. [ ] Consider reading and watching this workshop ([Description](https://docs.google.com/document/d/1HnT2CCtaE7MMD7Qkd8_MLugwICDqu9Lp3f48o76Dzwk), [Video](https://drive.google.com/file/d/17JG1Tp6no6gea4y5CRbsXSNzNNHAlcFK/view)) about setup GitLab CI. It contains valuable info about Docker with Docker Machine and custom domain name.
1. [ ] Install GitLab Runner locally with [this tutorial](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/howto/runner.md).
       Be sure to follow the Docker related setup instructions. (Optionally: you can set up your Runners to run inside containers by following these [instructions](https://docs.gitlab.com/runner/install/docker.html#docker-image-installation-and-configuration))
1. [ ] Register your runner with the command: `gitlab-runner register -c <gdk-path>/gitlab-runner-config.toml`,
       choose `Docker` as an executor. This command will generate the `gitlab-runner-config.toml` file.
1. [ ] Run the GitLab Runner in the foreground with the command:
`gitlab-runner --log-level debug run --config <gdk-path>/gitlab-runner-config.toml`
1. [ ] If you have questions about the runner or you're stuck and need help, ask in `#g_runner` Slack channel.
1. [ ] Make your runner **[privileged](https://docs.gitlab.com/runner/executors/docker.html#use-docker-in-docker-with-privileged-mode)** (`<gdk-path>/gitlab-runner-config.toml`: `privileged = true`). You may need this setting to run
       "Docker in Docker". It can be a requirement for secure jobs as the [SAST analyzers](https://gitlab.com/gitlab-org/security-products/analyzers) run as docker containers, which are spawned by the [orchestrator](https://gitlab.com/gitlab-org/security-products/analyzers/common/blob/master/orchestrator/orchestrator.go#L49). Since the orchestrator itself is running inside a container, we need to use the docker-in-docker paradigm.
       [Read more about it](https://docs.gitlab.com/runner/executors/docker.html#use-docker-in-docker-with-privileged-mode)

</details>

#### Extras

<details>
<summary>Click to expand/contract</summary>

1. [ ] If you are using [tmuxinator](https://github.com/tmuxinator/tmuxinator) then consider using [this](tmuxinator/gitlab.yml) configuration file to automate starting and stopping everything.

#### Kubectl and GCP

Our Defend features are tightly integrated into GitLab's [Cluster Integration feature](https://docs.gitlab.com/ee/user/project/clusters/) and often require us to work with clusters directly. As we are a multicloud application we support various cloud providers but primarily GCP. As such, this configuration is focused around GCP [although more AWS Integrations are coming](https://gitlab.com/groups/gitlab-org/-/epics/1328).

1. [ ] [Install Google Cloud SDK](https://cloud.google.com/sdk/docs/downloads-interactive)
1. [ ] Authenticate using your GCP credentials
1. [ ] Install `kubectl`: `gcloud components install kubectl`
1. [ ] [Install `helm` v2](https://helm.sh/docs/intro/install/) (Note: v3 is not yet tested)

</details>

### Minikube and local registry on macOS

<details>
<summary>Click to expand/contract</summary>

#### Pre-requisite
This short guide was written and tested with the following configuration:

- macOS Catalina: 10.15.1 (19B88)
- Docker engine: 19.03.5 (through docker desktop for mac 2.1.05)
- Minikube: v1.5.2
- Kubectl: v1.15.6

#### Install dependencies

- [ ] [Docker](https://hub.docker.com/editions/community/docker-ce-desktop-mac)
- [ ] [Minikube](https://kubernetes.io/docs/tasks/tools/install-minikube/)
- [ ] [Kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/#install-kubectl-on-macos).
- [ ] [GitLab Development Kit](#gdk)
- [ ] [GitLab Runner](#runner)
- [ ] [Enable local
  registry](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/howto/registry.md).
  Skip `docker-machine` steps.
- [ ] Add the registry IP address and port that you used above to the insecure registries in the
  Docker configuration.


#### Gitlab Runner in privileged mode

- [ ] Make sure the configuration file has privileged set to true and that you share your local
  docker daemon file with gitlab runner. I have used `docker:stable` as image and runner settings
  for cache to be secure. It should look something similiar to this:

```
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "main-runner"
  url = "<YOUR_IP>:<YOUR_PORT>"
  token = "<YOUR_TOKEN>"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.docker]
    tls_verify = false
    image = "docker:stable"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/var/run/docker.sock:/var/run/docker.sock","/cache"]
    shm_size = 0
  [runners.cache]
    Insecure = false
    [runners.cache.s3]
    [runners.cache.gcs]
```

Note: ```gdk restart``` is required when changing ```gitlab.yml``` file. The same goes for ```gitlab-runner restart``` and ```config.toml```. Sometimes when trying different configurations it is easy to miss.

#### Local cluster setup

- [ ] Start a new cluster. Replace ```<REGISTRY_IP>``` and ```<REGISTRY_PORT>``` with the information from the [Install dependencies](#install-dependencies) local registry step:

```
minikube start --memory=8192 --cpus=4 --kubernetes-version='v1.15.6' --insecure-registry="<REGISTRY_IP>:<REGISTRY_PORT>"
```
Note: Kubernetes v1.16.x [deprecated](https://kubernetes.io/blog/2019/07/18/api-deprecations-in-1-16/) some required extensions.



- [ ] In your local instance of gitlab, go to Admin->Settings->Network and make sure those options are checked:
```
Allow requests to the local network from web hooks and services
Allow requests to the local network from system hooks 
```
- [ ] Now you are ready to follow the same steps as described in the [adding an existing cluster](https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html#add-existing-cluster) guide.
- [ ] After installing Helm and Ingress, create a tunnel so that any load balancer with pending external ips can be provided with one. This can achieved with:

```
minikube tunnel
```

- [ ] After installing prometheus and refreshing the page, the metrics should be available in the UI and your environment should be ready to go.

Cluster API URL & IP can be found with the following commands:
```
$ kubectl cluster-info | grep 'Kubernetes master' | awk '/http/ {print $NF}'
https://192.168.64.8:8443
$ minikube ip
192.168.64.8
```

#### Next Step
- [ ] Import [waf-enablement-demo](https://gitlab.com/gitlab-org/defend/waf-enablement-demo) or similar projects into your local instance of gitlab. Trigger a new pipeline through CI/CD->Pipelines and the build and deploy stages should succeed.

</details>

</details>

### Day 7: Defend playground

<details>
<summary>Click to expand/contract</summary>

At this step you should have on your local machine:
 * installed gdk and running a local instance of GitLab EE (gdk run),
 * running and connected to GitLab CI Runner.


#### Auto DevOps (on gitlab.com)

<details>
<summary>Click to expand/contract</summary>

GitLab can automatically configure the entire CI/CD pipeline for projects (without the need to create the `.gitlab-ci.yml` file), this feature is known as [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/index.html)

Auto DevOps may seem complex but uses the same underlying CI configuration as our customers. You can easily examine [the ADO template that we ship with each version of GitLab](https://gitlab.com/gitlab-org/gitlab/blob/master/lib/gitlab/ci/templates/Auto-DevOps.gitlab-ci.yml).

Auto DevOps will automatically build, test, and deploy your application if all prerequisites are satisfied. We will start with simply building and testing your application on gitlab.com.

As part of this task you need to:
1. [ ] Create a new Express project on GitLab.com
1. [ ] Create Kubernetes cluster for your project by following this [guide](https://docs.gitlab.com/ee/topics/autodevops/quick_start_guide.html), your GitLab google account should already have access to the `group-defend` [GCP](https://cloud.google.com) project; create a cluster in the `group-defend` project. Consider using preemtible VMs (when creating new cluster, go to Node Pools -> default-pool -> Nodes -> Enable preemptible nodes) and decrease number of nodes used in your cluster.
1. [ ] Install the required [GitLab Managed Apps](https://docs.gitlab.com/ee/user/clusters/applications.html) as mentioned in the documentation linked above: Helm, Ingress, and Runner.
1. [ ] In your project go to ` Settings > CI/CD > Auto DevOps` and enable it. This should automatically spawn a new pipeline.
1. [ ] Make sure your pipeline (`CI/CD > Pipelines`) has an `Auto DevOps` label on it and that it eventually passes.
1. [ ] Visit the `Operations > Environments` page and "Open live environment" to view your newly deployed application.
1. [ ] Visit `Operations > Pod Logs` to view logs from your application
1. [ ] If you have installed Prometheus, visit `Operations > Metrics` to view metrics for your cluster and application.
1. [ ] If you are not going to work on Auto DevOps remember to remove your cluster from [GCP](https://cloud.google.com).

</details>

#### Auto DevOps (GDK)

<details>
<summary>Click to expand/contract</summary>

To enable Auto DevOps using GDK we must use a slightly different configuration than the standard GDK setup. This setup allows your local machine to verify a connection to GCP and our hosted infrastructure to allow creation, management, and deployment of clusters and resources.

1. [ ] [Follow the Auto DevOps HowTo guide](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/howto/auto_devops.md) for configuring your GDK properly.
1. [ ] Request access via SSH tunnel by following the instructions in the [Prerequisite section](https://gitlab.com/gitlab-org/gitlab-development-kit/blob/master/doc/howto/auto_devops.md#prerequisites-for-gitlab-employees-only) for Auto DevOps on GCP
1. [ ] Follow the [above steps](#auto-devops-on-gitlab-com) for creating an Express project using your local gitlab instance

</details>

#### Create Vulnerabilities

<details>
<summary>Click to expand/contract</summary>

When working in the [Vulnerability Management
Category](https://gitlab.com/gitlab-org/gitlab/-/issues?label_name[]=Category%3AVulnerability+Management),
as part of the [Threat Insights
group](https://about.gitlab.com/handbook/product/categories/#threat-insights-group), you will most
definitely need data for
[Findings](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/49901/diffs#7bf6094221c03a178b4f42bd0881b98b630260e4_0_64)
and
[Vulnerabilities](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/49901/diffs#7bf6094221c03a178b4f42bd0881b98b630260e4_0_144).

The steps below allow you to generate the required data. They assume you have configured GDK,
Runner and can run the GitLab rails application on your local dev environment.

1. Clone the [Security Reports](https://staging.gitlab.com/secure-team-test/security-reports) and
   [Dependency List Test](https://staging.gitlab.com/secure-team-test/dependency-list-test)
   repositories to your local GDK.
1. Run a pipeline on the `master` branch.
1. The pipelines should succeed and create/update vulnerabilities.

##### Create special case vulnerabilities

Sometimes one needs a vulnerability that can be resolved by a Merge Request on the vulnerability
page; unfortunately not all vulnerabilities have this capability. Here is a way to produce the
necessary data to use this feature.

1. Clone the [Yarn Remediation](https://staging.gitlab.com/secure-team-test/yarn-remediation) repo
1. Run a pipeline on the `cureable` branch.
1. Navigate to the project security dashboard select a vulnerability and view the details page. The
   `Resolve with MR` button should be available.
1. Click on the `Resolve with MR` button. A Merge Request is created and you are navigated to that page.

</details>


#### Interacting directly with cluster

<details>
<summary>Click to expand/contract</summary>

We have now deployed an application and gitlab managed applications using GitLab's UI. Occasionally we must debug or access the cluster directly to tail logs, check events, create/destroy resources. To do this we can connect directly to the cluster using kubectl and helm.

1. [ ] Pull credentials for the newly created cluster into your kubectl context: `gcloud container clusters get-credentials <my_cluster_name> --zone <my_zone> --project group-defend-c8e44e`
1. [ ] Verify `kubectl` is properly connected with `kubectl get pods --all-namespaces`
1. [ ] (Optionally) setting up `helm`

Helm can be connected directly to your cluster as well however tiller is currently installed with mutual SSL, so we must fetch our cert chain via kubectl. Here's a handy shell function for doing so**

```
function gitlab-helm() {
  mkdir -p ~/.helm
  [ -f ~/.helm/tiller-ca.crt ] || (kubectl get secrets/tiller-secret -n gitlab-managed-apps -o "jsonpath={.data['ca\.crt']}"  | base64 --decode > ~/.helm/tiller-ca.crt)
  [ -f ~/.helm/tiller.crt ]    || (kubectl get secrets/tiller-secret -n gitlab-managed-apps -o "jsonpath={.data['tls\.crt']}" | base64 --decode > ~/.helm/tiller.crt)
  [ -f ~/.helm/tiller.key ]    || (kubectl get secrets/tiller-secret -n gitlab-managed-apps -o "jsonpath={.data['tls\.key']}" | base64 --decode > ~/.helm/tiller.key)
  helm "$@" --tiller-connection-timeout 1 --tls \
    --tls-ca-cert ~/.helm/tiller-ca.crt --tls-cert ~/.helm/tiller.crt \
    --tls-key ~/.helm/tiller.key \
    --tiller-namespace gitlab-managed-apps
}
```
** **CAVEAT:** you are pulling in your certs here and not purging them when switching cluster/project contexts. These can be purged using another handy shim:

```
function gitlab-helm-purge-credentials() {
  [ -f ~/.helm/tiller-ca.crt ] && (rm -rf ~/.helm/tiller-ca.crt)
  [ -f ~/.helm/tiller.crt ]    && (rm -rf ~/.helm/tiller.crt)
  [ -f ~/.helm/tiller.key ]    && (rm -rf ~/.helm/tiller.key)
}
```

Using this command we can now fetch the helm releases tied to deployment:

```
gitlab ❯ gitlab-helm ls                                                                                        
NAME      	REVISION	UPDATED                 	STATUS  	CHART               	APP VERSION	NAMESPACE
ingress   	1       	Tue Nov  5 15:57:28 2019	DEPLOYED	nginx-ingress-1.22.1	0.25.1     	gitlab-managed-apps
prometheus	1       	Thu Nov  7 14:08:17 2019	DEPLOYED	prometheus-6.7.3    	2.2.1      	gitlab-managed-apps
runner    	1       	Tue Nov  5 15:57:32 2019	DEPLOYED	gitlab-runner-0.10.1	12.4.1     	gitlab-managed-apps
```

</details>

#### Prepare to contribute

<details>
<summary>Click to expand/contract</summary>

1. [ ] Configure GPG to [sign your commits](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/).
1. [ ] Create an MR with improvements to this [document](https://gitlab.com/gitlab-org/defend/onboarding/blob/master/.gitlab/issue_templates/Technical_Onboarding.md).
1. [ ] You are unlikely to work on a security fix as part of your first Deliverables, but once you are assigned to one please refer to the [security issue workflow](https://about.gitlab.com/handbook/engineering/workflow/#security-issues) and remember it is okay to ask questions.

</details>

#### Familiarize yourself with Defend product features 

<details>
<summary>Click to expand/contract</summary>

Review documentation for each group within Defend to begin to understand the features you will be supporting
##### Threat Insights
1. [ ] [Security Dashboards](https://docs.gitlab.com/ee/user/application_security/security_dashboard/)
1. [ ] [Vulnerability Details Page](https://docs.gitlab.com/ee/user/application_security/vulnerabilities/)
1. [ ] [Vulnerability APIs](https://docs.gitlab.com/ee/api/vulnerabilities.html)
##### Container Security
1. [ ] [Threat Monitoring](https://docs.gitlab.com/ee/user/application_security/threat_monitoring/)
1. [ ] [WAF](https://docs.gitlab.com/ee/topics/web_application_firewall/)

</details>

</details>


### Now you are ready for new tasks
You're awesome!
